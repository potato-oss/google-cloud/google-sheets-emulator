import unittest
import openpyxl
import os
import uuid
import tempfile
import time
from unittest import TestCase as BaseTestCase

from google.auth.credentials import AnonymousCredentials
from google.api_core.client_options import ClientOptions
from googleapiclient.discovery import build
from server import create_server


class TestCase(BaseTestCase):
    def setUp(self):
        self._data_dir = tempfile.mkdtemp()
        self._server = create_server(
            "localhost", 9077,
            data_dir=self._data_dir
        )

        self._server.start()
        time.sleep(3)

        credentials = AnonymousCredentials()
        self._drive_client = build(
            "drive",
            "v3",
            client_options=ClientOptions(api_endpoint="http://localhost:9077"),
            credentials=credentials
        )

        self._client = build(
            "sheets",
            "v4",
            client_options=ClientOptions(api_endpoint="http://localhost:9077"),
            credentials=credentials
        )

    def tearDown(self):
        self._server.stop()


class SpreadsheetValuesBatchGetTest(TestCase):

    def test_read_cell_range(self):
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.title = "One"

        ws["A1"] = "A1"
        ws["B1"] = "B1"
        ws["C1"] = "C1"
        ws["A2"] = "A2"
        ws["B2"] = "B2"
        ws["C2"] = "C2"

        spreadsheet_id = uuid.uuid4()
        filename = os.path.join(self._data_dir, "%s.xlsx" % spreadsheet_id)
        wb.save(filename)

        data = self._client.spreadsheets().values().batchGet(
            spreadsheetId=spreadsheet_id,
            ranges=["One!A1:C1", "One!A2:C2"]
        ).execute()

        self.assertEqual([x['range'] for x in data['valueRanges']], ['A1:C1', 'A2:C2'])
        self.assertEqual(data['valueRanges'][0]['values'], [['A1', 'B1', 'C1']])
        self.assertEqual(data['valueRanges'][1]['values'], [['A2', 'B2', 'C2']])


class SpreadsheetValuesTest(TestCase):

    def test_read_cell_range(self):
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.title = "One"

        ws["A1"] = "A1"
        ws["B1"] = "B1"
        ws["C1"] = "C1"
        ws["A2"] = "A2"
        ws["B2"] = "B2"
        ws["C2"] = "C2"

        spreadsheet_id = uuid.uuid4()
        filename = os.path.join(self._data_dir, "%s.xlsx" % spreadsheet_id)
        wb.save(filename)

        data = self._client.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id,
            range="One!A1:C1"
        ).execute()

        self.assertEqual(data['range'], 'A1:C1')
        self.assertEqual(data['values'], [['A1', 'B1', 'C1']])

    def test_read_cell_range_unbounded(self):
        wb = openpyxl.Workbook()
        ws = wb.active
        ws.title = "One"

        ws["A1"] = "A1"
        ws["B1"] = "B1"
        ws["C1"] = "C1"
        ws["A2"] = "A2"
        ws["B2"] = "B2"
        ws["C2"] = "C2"

        spreadsheet_id = uuid.uuid4()
        filename = os.path.join(self._data_dir, "%s.xlsx" % spreadsheet_id)
        wb.save(filename)

        data = self._client.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id,
            range="One!A1:C"
        ).execute()

        self.assertEqual(data['range'], 'A1:C2')
        self.assertEqual(data['values'], [['A1', 'B1', 'C1'], ['A2', 'B2', 'C2']])


if __name__ == '__main__':
    unittest.main()
