import os
import string
import logging
import threading
import time
import tempfile
import openpyxl
import uvicorn
from fastapi import FastAPI, HTTPException, Query
from fastapi.responses import HTMLResponse
from typing import List, Optional

# Time to sleep between iterations of the threads
_LOOP_SLEEP_TIME = 0.1

logger = logging.getLogger(__file__)


DATA_DIR = None

app = FastAPI()


def parse_cell(cell):
    col = ""
    row = ""

    for i, c in enumerate(cell):
        if c in string.ascii_uppercase:
            col += c
        else:
            break
    else:
        i += 1

    row = cell[i:]
    return col, row


def _fetch_range(book, range):
    sheet, cells = range.split("!", 1)
    start, end = cells.split(":", 1)

    if sheet not in book.sheetnames:
        raise HTTPException(
            status_code=404, detail="Worksheet not found"
        )

    ws = book[sheet]

    # It's possible the ranges are unbounded (e.g. A1:C, rather than
    # A1:C2 - openpyxl can't deal with that so we rebuild the range
    # string using the max_row of the ws if no row was specified)
    scol, srow = parse_cell(start)
    ecol, erow = parse_cell(end)

    erow = erow or ws.max_row
    srow = srow or ws.max_row

    cells = "%s%s:%s%s" % (scol, srow, ecol, erow)
    start, end = cells.split(":", 1)

    cells = ws[start:end]

    return {
        "range": "%s:%s" % (start, end),
        "values": [[col.value for col in row] for row in cells]
    }

@app.get("/v4/spreadsheets/{spreadsheet_id}/values:batchGet")
async def spreadsheets(spreadsheet_id: str, ranges: List[str] = Query([])):
    filename = os.path.join(DATA_DIR, "%s.xlsx" % spreadsheet_id)

    if not os.path.exists(filename):
        raise HTTPException(
            status_code=404, detail="Spreadsheet not found"
        )

    book = openpyxl.load_workbook(filename)

    range_values = []
    for range in ranges:
        range_values.append(_fetch_range(book, range))

    return {
        "spreadsheetId": spreadsheet_id,
        "valueRanges": range_values
    }


@app.get("/v4/spreadsheets/{spreadsheet_id}/values/{range}")
async def spreadsheets(spreadsheet_id: str, range: str):
    filename = os.path.join(DATA_DIR, "%s.xlsx" % spreadsheet_id)

    if not os.path.exists(filename):
        raise HTTPException(
            status_code=404, detail="Spreadsheet not found"
        )

    book = openpyxl.load_workbook(filename)
    data = _fetch_range(book, range)

    return data


@app.get("/v4/spreadsheets/{spreadsheet_id}")
async def spreadsheet(spreadsheet_id: str):
    filename = os.path.join(DATA_DIR, "%s.xlsx" % spreadsheet_id)

    if not os.path.exists(filename):
        raise HTTPException(
            status_code=404, detail="Spreadsheet not found"
        )

    book = openpyxl.load_workbook(filename)

    def sheet_data(sheet, i):
        return {
            "spreadsheetId": spreadsheet_id,
            "properties": {
                "sheetId": i,
                "title": sheet.title,
                "index": i,
            }
        }

    data = {
        "spreadsheetId": spreadsheet_id,
        "properties": {},
        "sheets": [sheet_data(sheet, i) for i, sheet in enumerate(book.worksheets)],
        "namedRanges": [],
        "spreadsheetURL": "",
    }

    return data


@app.get("/files")
async def drive_files():
    """
        https://developers.google.com/drive/api/v3/reference/files/list
    """
    response = []
    for fname in os.listdir(DATA_DIR):
        name, ext = os.path.splitext(fname)
        if ext == ".xlsx":
            response.append({
                "kind": "drive#file",
                "id": name,
                "name": name,
                "mimeType": "application/vnd.google-apps.spreadsheet"
            })

    meta = {
        "kind": "drive#fileList",
        "files": response,
        "incompleteSearch": False
    }

    return meta


@app.get("/", response_class=HTMLResponse)
def check():
    return "OK"


class APIThread(threading.Thread):
    def __init__(self, host: str, port: int, data_dir: str, *args, **kwargs):
        global DATA_DIR

        super().__init__(*args, **kwargs)

        self._host = host
        self._port = port
        self._data_dir = DATA_DIR = data_dir
        self._is_running = threading.Event()
        self._httpd = None

        if self._data_dir is None:
            self._data_dir = tempfile.mkdtemp()
        elif not os.path.exists(self._data_dir):
            logger.info("[API] Creating folder at %s", self._data_dir)
            os.makedirs(self._data_dir)

    def run(self):
        logger.info("[API] Starting API server at %s:%s", self._host, self._port)
        config = uvicorn.Config(app, host=self._host, port=self._port, workers=1)
        self._httpd = uvicorn.Server(config=config)
        self._httpd.run()

    def join(self, timeout=None):
        logger.info("[API] Stopping API server")
        if self._httpd:
            self._httpd.should_exit = True
        super().join(timeout)


class Server(object):
    def __init__(self, host, port, data_dir):
        self._api = APIThread(host, port, data_dir)

    def start(self):
        self._api.start()  # Start the API thread

    def stop(self):
        self._api.join(timeout=3)

    def run(self):
        try:
            self.start()

            logger.info("[SERVER] All services started")

            while True:
                try:
                    time.sleep(_LOOP_SLEEP_TIME)
                except KeyboardInterrupt:
                    break

        finally:
            self.stop()


def create_server(host, port, data_dir):
    return Server(host, port, data_dir)
