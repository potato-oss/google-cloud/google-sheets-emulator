# Local Emulator for Google Sheets

This is a basic emulator for Google Sheets to allow development
of code locally that deals with Google Sheets API

THIS IS NOT A COMPLETE IMPLEMENTATION!

Please submit merge requests for additional endpoints as you need them.


## Development

You can run the server locally while developing using tox:

```
tox -e run
```
